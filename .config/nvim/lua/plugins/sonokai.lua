local M = {
        'sainnhe/sonokai',
        lazy = false,
        config = function()
            vim.g.sonokai_style = 'espresso'
            vim.g.sonokai_enable_italic = true
        end
}

return M
