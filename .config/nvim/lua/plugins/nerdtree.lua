local M = {
    'preservim/nerdtree',
    lazy = false,
}

return M
