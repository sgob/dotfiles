local M = {
    'ap/vim-css-color',
    lazy = true,
    ft="css",
}

return M
